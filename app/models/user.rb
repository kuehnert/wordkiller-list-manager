class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable,
         :registerable # Users don't register themselves

  has_many :word_lists
  
  validates :user_name, :presence => true, :uniqueness => true
  
  def role_symbols
    [ :user ]
  end
end
