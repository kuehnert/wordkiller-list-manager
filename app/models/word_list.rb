class WordList < ActiveRecord::Base
  belongs_to :user

  validates :name, :presence => true, :uniqueness => true
  validates :user_id, :presence => true
  validates :good_words, :presence => true
  validates :bad_words, :presence => true
  
  scope :ordered, -> { order("LOWER(name)") }
end
