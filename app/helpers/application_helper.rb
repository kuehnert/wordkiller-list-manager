module ApplicationHelper
  def title(page_title, show_title = true)
    content_for :title, page_title.to_s
    @show_title = show_title
  end
  
  def show_title?
    @show_title
  end

  def flash_class(level)
    case level
    when :notice then "info"
    when :error then "error"
    when :alert then "warning"
    end
  end

  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.simple_fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end

  def lesc(text)
    LatexToPdf.escape_latex(text).force_encoding('UTF-8')
  end
  
  def textilize(text)
    Textilizer.new(text).to_html.html_safe unless text.blank?
  end
end
