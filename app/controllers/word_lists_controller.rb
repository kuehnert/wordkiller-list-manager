class WordListsController < InheritedResources::Base
  respond_to :html, :json
  filter_resource_access

  def index
    if request.format == 'json'
      Settings.play_count += 1
      Settings.last_played = Time.now
    else
      @play_count  = Settings.play_count
      @play_count_since = Settings.play_count_since.to_date
      @last_played = Settings.last_played
    end
    
    index!
  end

  def create
    # @word_list created by filter_resource_access
    @word_list.user = current_user
    
    create!
  end

  protected

  def collection
    @word_lists ||= end_of_association_chain.ordered
  end

  def permitted_params
    { word_list: params.fetch(:word_list, {}).permit(:name, :good_words, :bad_words) }
  end

  def new_word_list_from_params
    @word_list = WordList.new(permitted_params[:word_list])
  end
end
