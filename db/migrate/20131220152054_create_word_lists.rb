class CreateWordLists < ActiveRecord::Migration
  def change
    create_table :word_lists do |t|
      t.string :name
      t.references :user, index: true
      t.text :good_words
      t.text :bad_words

      t.timestamps
    end
  end
end
