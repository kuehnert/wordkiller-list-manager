if ActiveRecord::Base.connection.tables.include?('settings')
  Settings.defaults[:play_count] = 0
  Settings.save_default(:play_count_since, Time.now)
  Settings.save_default(:last_played, Time.now)
end
