role :app, %w{deploy@marienschule-opladen.com}
role :web, %w{deploy@marienschule-opladen.com}
role :db,  %w{deploy@marienschule-opladen.com}

server 'marienschule-opladen.com', user: 'deploy', roles: %w{web app}
