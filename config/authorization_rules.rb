authorization do
  role :guest do
    has_permission_on :word_lists, to: :read
    has_permission_on :devise_sessions, to: :manage
    has_permission_on :devise_registrations, to: :manage
    has_permission_on :devise_confirmations, to: :manage
  end
  
  role :user do
    has_omnipotence
  end
end

privileges do
  # privilege :read, :rooms, includes: :info
  # privilege :edit, :duties, includes: :edit_all
  # privilege :update, :duties, includes: :update_all
  
  privilege :manage, includes: [:create, :read, :update, :delete]
  privilege :read,   includes: [:index, :show]
  privilege :create, includes: :new
  privilege :update, includes: :edit
  privilege :delete, includes: :destroy
end
