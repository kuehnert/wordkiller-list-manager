lock '3.4.0'

set :application,   'wordkiller'
set :repo_url,      'git@bitbucket.org:kuehnert/wordkiller-list-manager.git'

# Default value for :log_level is :debug
# set :log_level, :info

set :linked_files, %w{config/database.yml}

set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :rbenv_path, '/home/deploy/.rbenv'

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart
  after :finishing, :cleanup
end
